# Signifyd for Drupal 8 & Commerce 2

This module is in development and not meant for production websites.

1. Enable the signifyd.
2. Navigate to admin/config/services/signifyd and place your api key.
3. Edit your shipping methods and associate a shipper and shipping method.
4. Implement hook_signifyd_case_data_alter to set your site specific data.
