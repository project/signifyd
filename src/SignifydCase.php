<?php

namespace Drupal\signifyd;


/**
 * Defines Signifyd api class.
 */
class SignifydCase {

  /**
   * The order.
   */
  protected $order;

  /**
   * The case.
   */
  protected $caseData;

  /**
   * Constructs a new Signifyd object.
   */
  public function __construct($order) {
    $this->order = $order;
    $this->caseData = $this->caseData();
  }

  public function submit() {
    $data = $this->caseData;
    $api_key = \Drupal::config('signifyd.settings')->get('api_key');
    \Drupal::moduleHandler()->alter('signifyd_case_data', $data);
    $response = \Drupal::httpClient()->post('https://api.signifyd.com/v2/cases', [
      'auth' => [$api_key, ''],
      'body' => json_encode($data),
    ])->getBody()->getContents();
    return $response;
  }

  public function caseData() {
    $case = [];
    $case['purchase'] = $this->getPurchase();
    if ($this->order->hasField('shipments') && !$this->order->get('shipments')->isEmpty()) {
      $case['recipients'] = $this->getRecipients();
    }
    $case['transactions'] = $this->getTransactions();
    $case['userAccount'] = $this->getUserAccount();
    return $case;
  }

  public function getPurchase() {
    $purchase = [
      "orderId" => $this->order->id(),
      "orderSessionId" => $this->orderSessionId(),
      "browserIpAddress" => $this->order->getIpAddress(),
      "createdAt" => date('Y-m-d\TH:i:sP', $this->order->getPlacedTime()),
      "currency" => $this->order->getTotalPaid()->getCurrencyCode(),
      "orderChannel" => "WEB",
      "totalPrice" => 10, //$this->order->getTotalPaid()->getNumber(),
      "products" => $this->getProducts(),
      "discountCodes" => $this->getDiscountCodes(),
    ];
    if ($this->order->hasField('shipments') && !$this->order->get('shipments')->isEmpty()) {
      $purchase['shipments'] = $this->getShipments();
    }
    return $purchase;
  }

  public function getProducts() {
    $products = [];
    foreach ($this->order->getItems() as $order_item) {
      $products[] = [
        "itemId" => $order_item->getPurchasedEntity()->getSku(),
        "itemName" => $order_item->getPurchasedEntity()->getTitle(),
        "itemIsDigital" => FALSE,
        "itemUrl" => $order_item->getPurchasedEntity()->toUrl()->setAbsolute()->toString(),
        //"itemImage" => "",
        "itemQuantity" => (int) $order_item->getQuantity(),
        "itemPrice" => $order_item->getUnitPrice()->getNumber(),
        //"weight" => $order_item->getPurchasedEntity()->weight->getValue()[0]['number'],
      ];
    }
    return $products;
  }

  public function getDiscountCodes() {
    $discount_codes = [];
    if (!$this->order->coupons->isEmpty()) {
      foreach ($this->order->coupons->referencedEntities() as $coupon) {
        $discount_code = [
          'code' => $coupon->getCode(),
        ];
        $offer = $coupon->getPromotion()->getOffer()->getConfiguration();
        if (isset($offer['percentage'])) {
          $discount_code['percentage'] = $offer['percentage'] * 100;
        }
        else if (isset($offer['amount'])) {
          $discount_code['amount'] = $offer['amount']['number'];
        }
        $discount_codes[] = $discount_code;
      }
    }
    return $discount_codes;
  }

  public function getShipments() {
    $shipments = $this->order->get('shipments')->referencedEntities();
    if (empty($shipments)) {
      return [];
    }
    $data = [];
    foreach ($shipments as $shipment_entity) {
      $shipment = [
        'shipper' => '',
        'shippingMethod' => 'STANDARD',
        'shippingPrice' => $shipment_entity->getAmount()->getNumber(),
      ];
      $shipping_method = $shipment_entity->getShippingMethod();
      if ($shipping_method->hasField('signifyd_shipper') && !$shipping_method->get('signifyd_shipper')->isEmpty()) {
        $shipment['shipper'] = $shipping_method->get('signifyd_shipper')->value;
      }
      if ($shipping_method->hasField('signifyd_shipping_method') && !$shipping_method->get('signifyd_shipping_method')->isEmpty()) {
        $shipment['shippingMethod'] = $shipping_method->get('signifyd_shipping_method')->value;
      }
      $data[] = $shipment;
    }
    return $data;
  }

  public function orderSessionId() {
    $host = \Drupal::request()->getHost();
    $orderSessionId = base64_encode($host . $this->order->id());
    return preg_replace('/[^a-zA-Z0-9_-]/', '', $orderSessionId);
  }

  public function getRecipients() {
    $shipments = $this->order->get('shipments')->referencedEntities();
    if (empty($shipments)) {
      return [];
    }
    $shipment = reset($shipments);
    $profile = $shipment->getShippingProfile()->address->getValue();
    $profile = reset($profile);
    return [
      [
        "fullName" => "{$profile['given_name']} {$profile['family_name']}",
        "confirmationEmail" => $this->order->getEmail(),
        "confirmationPhone" => "",
        "organization" => $profile['organization'],
        "deliveryAddress" => [
          "streetAddress" => $profile['address_line1'],
          "unit" => $profile['address_line2'],
          "city" => $profile['locality'],
          "provinceCode" => $profile['administrative_area'],
          "postalCode" => $profile['postal_code'],
          "countryCode" => $profile['country_code'],
        ]
      ]
    ];
  }

  public function getTransactions() {
    $payments = \Drupal::entityTypeManager()
      ->getStorage('commerce_payment')
      ->loadMultipleByOrder($this->order);
    $transactions = [];
    foreach ($payments as $payment) {
      $transaction = [
        'transactionId' => $payment->id(),
        'type' => 'SALE',
        'gatewayStatusCode' => 'SUCCESS',
        'currency' => $payment->getAmount()->getCurrencyCode(),
        'amount' => $payment->getAmount()->getNumber(),
      ];
      $transactions[] = $transaction;
    }
    return $transactions;
  }

  public function getUserAccount() {
    $account = $this->order->getCustomer();
    return [
      "email" => $account->getEmail(),
      "username" => $account->getAccountName(),
      // "phone" => "",
      "createdDate" => date('Y-m-d\Th:i:sP', $account->getCreatedTime()),
      "accountNumber" => $account->id(),
      //"lastOrderId" => "",
      // "aggregateOrderCount" => 0,
      // "aggregateOrderDollars" => 0,
      "lastUpdateDate" => date('Y-m-d\Th:i:sP', $account->getChangedTime()),
      // "rating" => ""
    ];
  }

}
