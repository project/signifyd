<?php

namespace Drupal\signifyd\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\signifyd\SignifydCase;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class OrderPaymentSubscriber implements EventSubscriberInterface {

  /**
   * The state key/value store.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The order storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $orderStorage;

  /**
   * Constructs a new OrderPlacedEventSubscriber object.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state key/value store.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function __construct(StateInterface $state, EntityTypeManagerInterface $entity_type_manager) {
    $this->state = $state;
    $this->orderStorage = $entity_type_manager->getStorage('commerce_order');
  }

  /**
   * Flags the order event to submit to Signify.
   *
   * @param \Drupal\commerce_order\Event\OrderEvent $event
   *   The order event.
   */
  public function flagOrderEvent(OrderEvent $event) {
    $this->flag($event->getOrder());
  }

  /**
   * Flags the workflow event to submit to Signify.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The workflow transition event.
   */
  public function flagWorkflowEvent(WorkflowTransitionEvent $event) {
    $entity = $event->getEntity();
    $order = NULL;
    switch ($entity->getEntityTypeId()) {
      case 'commerce_payment':
        $order = $entity->getOrder();
        break;
      case 'commerce_order':
        $order = $entity;
        break;
    }
    $this->flag($order);
  }

  /**
   * Flags the order to submit to Signify.
   */
  public function flag($order) {
    $existing = $this->state->get('signifyd_orders', []);
    $existing[$order->id()] = $order->id();
    $this->state->set('signifyd_orders', $existing);
  }

  /**
   * Sends case data from orders to Signifyd.
   *
   * @param \Symfony\Component\HttpKernel\Event\PostResponseEvent $event
   *   The post response event.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */

  public function submitCase(PostResponseEvent $event) {
    $order_ids = $this->state->get('signifyd_orders', []);
    $orders = $this->orderStorage->loadMultiple($order_ids);
    foreach ($orders as $order) {
      $case = new SignifydCase($order);
      $case->submit();
      unset($order_ids[$order->id()]);
    }
    $this->state->set('signifyd_orders', $order_ids);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      'commerce_order.place.post_transition' => 'flagWorkflowEvent',
      'commerce_order.fulfill.post_transition' => 'flagWorkflowEvent',
      'commerce_order.cancel.post_transition' => 'flagWorkflowEvent',
      'commerce_order.order.paid' => 'flagOrderEvent',
      'commerce_payment.authorize.post_transition' => 'flagWorkflowEvent',
      'commerce_payment.void.post_transition' => 'flagWorkflowEvent',
      'commerce_payment.expire.post_transition' => 'flagWorkflowEvent',
      'commerce_payment.authorize_capture.post_transition' => 'flagWorkflowEvent',
      'commerce_payment.capture.post_transition' => 'flagWorkflowEvent',
      'commerce_payment.partially_refund.post_transition' => 'flagWorkflowEvent',
      'commerce_payment.refund.post_transition' => 'flagWorkflowEvent',
      'commerce_payment.create.post_transition' => 'flagWorkflowEvent',
      'commerce_payment.receive.post_transition' => 'flagWorkflowEvent',
      KernelEvents::TERMINATE => 'submitCase',
    ];
  }

}
