<?php

/**
 * @file
 * Hooks provided by the Signifyd module.
 */

/**
 * Perform alterations before case data is sent to Signifyd.
 *
 * @param array $data
 *   The case data sent to Signifyd.
 */
function hook_signifyd_case_data_alter(array &$data) {
  // Make alterations to the case data with site specific details such as
  // payment gateway responses, shipping, etc.
}
